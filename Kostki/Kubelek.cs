﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kostki
{
    class Kubelek
    {
        int IleKostek;
        int IleScian;
        public Kubelek(int ileKostek, int ileScian)
        {
            this.IleScian = ileScian;
            this.IleKostek = IleKostek;
        }

        public List<int> rzut()
        {
            List<int> lista = new List<int>(IleKostek);
            Random rand = new Random();
            for (int i = 0; i < this.IleKostek; i++)
            {
                lista.Add(rand.Next(0, IleScian));
            }
            return lista;
        }
    }
}
